# OpenML dataset: jungle_chess_2pcs_endgame_panther_lion

https://www.openml.org/d/40998

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

### Description ###

This dataset is part of a collection datasets based on the game "Jungle Chess" (a.k.a. Dou Shou Qi). For a description of the rules, please refer to the paper (link attached). The paper also contains a description of various constructed features. As the tablebases are a disjoint set of several tablebases based on which (two) pieces are on the board, we have uploaded all tablebases that have explicit different content:

* Rat vs Rat
* Rat vs Panther
* Rat vs. Lion
* Rat vs. Elephant
* Panther vs. Lion
* Panther vs. Elephant
* Tiger vs. Lion
* Lion vs. Lion
* Lion vs. Elephant
* Elephant vs. Elephant
* Complete (Combination of the above)
* RAW Complete (Combination of the above, containing for both pieces just the rank, file and strength information). This dataset contains a similar classification problem as, e.g., the King and Rook vs. King problem and is suitable for classification tasks. 

(Note that this dataset is one of the above mentioned datasets). Additionally, note that several subproblems are very similar. Having seen a given positions from one of the tablebases arguably gives a lot of information about the outcome of the same position in the other tablebases. 

### Please cite ###
J. N. van Rijn and J. K. Vis, Endgame Analysis of Dou Shou Qi. ICGA Journal 37:2, 120--124, 2014. ArXiv link: https://arxiv.org/abs/1604.07312

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/40998) of an [OpenML dataset](https://www.openml.org/d/40998). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/40998/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/40998/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/40998/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

